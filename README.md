# eslint config

A common eslint config for use in ta-interaktiv projects.

## Installation

Add config and peer dependencies:

```bash
yarn add @ta-interaktiv/eslint-config --dev
```

Then add a the following to `.eslintrc`:

```json
{
  "extends": "@ta-interaktiv"
}
```

Why is this so complicated? Because the [maintainers at eslint think that 
this solution is actually semantically purer than defining these things as 
actual dependencies, which they are]().

## Supports
* [StandardJS](https://standardjs.com)
* JSX
* [Flow](https://flow.org)
* [Jest](http://facebook.github.io/jest/)